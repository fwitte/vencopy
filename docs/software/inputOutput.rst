.. VencoPy documentation source file, created for sphinx

.. _inputOutput:


Input-Output
===================================

In the VencoPy inputs and outputs, we can differentiate between configuration files, disk files and classes outputs.
Moreover, we can distinguish between the overall framework IO and the classes IO.


General Framework Input-Output
---------------------------------------------------
**Inputs:**

 * National travel surveys
 * Mobility patterns from traffic models


**Outputs:**

 * Refer to the outputs of the :ref:`flexEstimator` class for more details




Classes Input-Output
---------------------------------------------------

 * :ref:`dataParser`
 * :ref:`tripDiaryBuilder`
 * :ref:`gridModeler`
 * :ref:`flexEstimator`
